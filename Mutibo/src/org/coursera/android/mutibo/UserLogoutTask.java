package org.coursera.android.mutibo;

import java.lang.ref.WeakReference;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.widget.Toast;

/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserLogoutTask extends AsyncTask<Void, Void, Boolean> {

		final GamePlayActivity gamePlayActivity;
		
		UserLogoutTask(GamePlayActivity outer) {
			gamePlayActivity = new WeakReference<GamePlayActivity>(outer).get();
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			try {
				// Simulate network access.
				Thread.sleep(2000);
				//TODO: perform logout logic here
				SharedPreferences sharedPref = PreferenceManager
						.getDefaultSharedPreferences(gamePlayActivity);
				SharedPreferences.Editor editor = sharedPref.edit();
				editor.putBoolean(SettingsActivity.KEY_PREF_KEEP_ME, false);
				editor.commit();
				return true;
			} catch (InterruptedException e) {
				return false;
			}
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			gamePlayActivity.showProgress(false);
			if (success) {
				gamePlayActivity.startLoginActivity();
				gamePlayActivity.finish();
			} else {
				Toast.makeText(gamePlayActivity, "Unable to complete action", Toast.LENGTH_SHORT).show();
			}
		}

		@Override
		protected void onCancelled() {
			gamePlayActivity.showProgress(false);
		}
	}
