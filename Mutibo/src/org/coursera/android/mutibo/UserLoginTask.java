package org.coursera.android.mutibo;

import java.lang.ref.WeakReference;

import android.content.Intent;
import android.os.AsyncTask;

/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

		final LoginActivity loginActivity;
		private final String mEmail;
		private final String mPassword;
		
		/**
		 * A dummy authentication store containing known user names and passwords.
		 * TODO: remove after connecting to a real authentication system.
		 */
		private static final String[] DUMMY_CREDENTIALS = new String[] {
				"foo@example.com:hello", "bar@example.com:world" };

		UserLoginTask(LoginActivity outer, String email, String password) {
			mEmail = email;
			mPassword = password;
			loginActivity = new WeakReference<LoginActivity>(outer).get();
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO: attempt authentication against a network service.

			try {
				// Simulate network access.
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				return false;
			}

			for (String credential : DUMMY_CREDENTIALS) {
				String[] pieces = credential.split(":");
				if (pieces[0].equals(mEmail)) {
					// Account exists, return true if the password matches.
					return pieces[1].equals(mPassword);
				}
			}

			// TODO: register the new account here.
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			loginActivity.mAuthTask = null;
			loginActivity.showProgress(false);

			if (success) {
				loginActivity.startGame();
			} else {
				loginActivity.mPasswordView
						.setError(loginActivity.getString(R.string.error_incorrect_password));
				loginActivity.mPasswordView.requestFocus();
			}
		}

		@Override
		protected void onCancelled() {
			loginActivity.mAuthTask = null;
			loginActivity.showProgress(false);
		}
	}
