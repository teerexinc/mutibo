package org.magnum.mobilecloud.movie;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.magnum.mobilecloud.movie.repository.Movie;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This is a utility class to aid in the construction of Video objects with
 * random names, urls, and durations. The class also provides a facility to
 * convert objects into JSON using Jackson, which is the format that the
 * VideoSvc controller is going to expect data in for integration testing.
 * 
 * @author jules
 *
 */
public class TestData {

	private static final ObjectMapper objectMapper = new ObjectMapper();

	/**
	 * Construct and return a Movie object with a random name, year, and
	 * recievedOscar.
	 * 
	 * @return
	 */
	public static Movie randomMovie() {
		String xmlfile = "/users/dell/desktop/mutibo/movies.xml";
		List<Movie> movies = MoviesXMLHandler.loadMoviesFromFile(xmlfile);
		Random r = new Random();
		int id = r.nextInt(440);
		return movies.get(id);
	}

	/**
	 * Convert an object to JSON using Jackson's ObjectMapper
	 * 
	 * @param o
	 * @return
	 * @throws Exception
	 */
	public static String toJson(Object o) throws Exception {
		return objectMapper.writeValueAsString(o);
	}
}