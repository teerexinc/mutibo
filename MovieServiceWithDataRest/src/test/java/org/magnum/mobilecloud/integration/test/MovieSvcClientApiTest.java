package org.magnum.mobilecloud.integration.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.magnum.mobilecloud.movie.TestData;
import org.magnum.mobilecloud.movie.client.MovieSvcApi;
import org.magnum.mobilecloud.movie.repository.Movie;

import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;

/**
 * 
 * This integration test sends a POST request to the MovieServlet to add a new
 * movie and then sends a second GET request to check that the video showed up
 * in the list of movies. Actual network communication using HTTP is performed
 * with this test.
 * 
 * The test requires that the MovieSvc be running first (see the directions in
 * the README.md file for how to launch the Application).
 * 
 * To run this test, right-click on it in Eclipse and select
 * "Run As"->"JUnit Test"
 * 
 * Pay attention to how this test that actually uses HTTP and the test that just
 * directly makes method calls on a MovieSvc object are essentially identical.
 * All that changes is the setup of the movieService variable. Yes, this could
 * be refactored to eliminate code duplication...but the goal was to show how
 * much Retrofit simplifies interaction with our service!
 * 
 * @author taiwo
 *
 */
public class MovieSvcClientApiTest {

	private final String TEST_URL = "http://localhost:8080";

	private MovieSvcApi movieService = new RestAdapter.Builder()
			.setEndpoint(TEST_URL).setLogLevel(LogLevel.FULL).build()
			.create(MovieSvcApi.class);

	private Movie movie = TestData.randomMovie();

	/**
	 * This test creates a Movie, adds the Movie to the MovieSvc, and then
	 * checks that the Movie is included in the list when getMovieList() is
	 * called.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testMovieAddAndList() throws Exception {

		// Add the video
		movieService.addMovie(movie);

		// We should get back the movie that we added above
		Collection<Movie> movies = movieService.getMovieList();
		assertTrue(movies.contains(movie));
	}
	
	@Test
	public void testFindByTitle() throws Exception {
		List<Movie> movies = (List<Movie>) movieService.findByActor("Carlos Guerrero");
		assertTrue(!movies.equals(null));
	}

}
