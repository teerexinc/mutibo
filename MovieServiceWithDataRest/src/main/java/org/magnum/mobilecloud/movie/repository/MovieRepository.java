package org.magnum.mobilecloud.movie.repository;

import java.util.Collection;

import org.magnum.mobilecloud.movie.client.MovieSvcApi;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * An interface for a repository that can store Movie
 * objects and allow them to be searched by title.
 * 
 * @author taiwo
 *
 */
// This @RepositoryRestResource annotation tells Spring Data Rest to
// expose the MovieRepository through a controller and map it to the 
// "/movie" path. This automatically enables you to do the following:
//
// 1. List all movies by sending a GET request to /movie 
// 2. Add a movie by sending a POST request to /movie with the JSON for a movie
// 3. Get a specific movie by sending a GET request to /movie/{movieId}
//    (e.g., /movie/1 would return the JSON for the movie with id=1)
// 4. Send search requests to our findByXYZ methods to /movie/search/findByXYZ
//    (e.g., /movie/search/findByTitle?title=Foo)
//
@RepositoryRestResource(path = MovieSvcApi.MOVIE_SVC_PATH)
public interface MovieRepository extends CrudRepository<Movie, Long>{

	// Find all videos with a matching title
	public Collection<Movie> findByTitle(
			// The @Param annotation tells Spring Data Rest which HTTP request
			// parameter it should use to fill in the "title" variable used to
			// search for Movies
			@Param(MovieSvcApi.TITLE_PARAMETER) String title);
	
	public Collection<Movie> findByYear(
			@Param(MovieSvcApi.YEAR_PARAMETER) int year);

	public Collection<Movie> findByActors(
				@Param(MovieSvcApi.ACTOR_PARAMETER) String actor);
	
	public Collection<Movie> findByGenres(
			@Param(MovieSvcApi.GENRE_PARAMETER) String genre);
	
	/*
	 * See: http://docs.spring.io/spring-data/jpa/docs/1.3.0.RELEASE/reference/html/jpa.repositories.html 
	 * for more examples of writing query methods
	 */
	
}
