package org.magnum.mobilecloud.movie.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.google.common.base.Objects;

/**
 * A simple object to represent a movie.
 * 
 * @author taiwo
 * 
 */
@Entity
public class Movie {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private String title;
	private int year;
	private int runtime;
	private String language;
	private String country;

	@ElementCollection
	@CollectionTable(name = "genres")
	private List<String> genres = new ArrayList<String>();

	@ElementCollection
	@CollectionTable(name = "directors")
	private List<String> directors = new ArrayList<String>();

	@ElementCollection
	@CollectionTable(name = "actors")
	private List<String> actors = new ArrayList<String>();

	public Movie() {
		super();
	}

	public Movie(int id, String title, int year, int runtime, String language,
			String country, List<String> genre, List<String> directors,
			List<String> cast) {
		super();
		this.id = id;
		this.title = title;
		this.year = year;
		this.runtime = runtime;
		this.language = language;
		this.country = country;
		this.genres = genre;
		this.directors = directors;
		this.actors = cast;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getRuntime() {
		return runtime;
	}

	public void setRuntime(int runtime) {
		this.runtime = runtime;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public List<String> getGenres() {
		return genres;
	}

	public void setGenres(List<String> genres) {
		this.genres = genres;
	}

	public List<String> getDirectors() {
		return directors;
	}

	public void setDirectors(List<String> directors) {
		this.directors = directors;
	}

	public List<String> getActors() {
		return actors;
	}

	public void setActors(List<String> actors) {
		this.actors = actors;
	}

	/**
	 * Two Videos will generate the same hashcode if they have exactly the same
	 * values for their title and year.
	 * 
	 */
	@Override
	public int hashCode() {
		// Google Guava provides great utilities for hashing
		return Objects.hashCode(id, title, year, runtime);
	}

	/**
	 * Two Movies are considered equal if they have exactly the same values for
	 * their title, and year.
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Movie) {
			Movie other = (Movie) obj;
			// Google Guava provides great utilities for equals too!
			return Objects.equal(title, other.title)
					&& Objects.equal(runtime, other.runtime)
					&& Objects.equal(year, other.year);
		} else {
			return false;
		}
	}

}