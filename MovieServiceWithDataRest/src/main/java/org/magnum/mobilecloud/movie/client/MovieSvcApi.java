package org.magnum.mobilecloud.movie.client;

import java.util.Collection;

import org.magnum.mobilecloud.movie.repository.Movie;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * This interface defines an API for a MovieSvc. The interface is used to
 * provide a contract for client/server interactions. The interface is annotated
 * with Retrofit annotations so that clients can automatically convert the
 * 
 * 
 * @author taiwo
 *
 */
public interface MovieSvcApi {

	public static final String TITLE_PARAMETER = "title";

	public static final String YEAR_PARAMETER = "year";

	public static final String ACTOR_PARAMETER = "actor";
	
	public static final String GENRE_PARAMETER = "genre";
	
	// The path where we expect the MovieSvc to live
	public static final String MOVIE_SVC_PATH = "/movie";

	// The path to search movies by title
	public static final String MOVIE_TITLE_SEARCH_PATH = MOVIE_SVC_PATH
			+ "/search/findByTitle";

	// The path to search movies by cast
	public static final String MOVIE_CAST_SEARCH_PATH = MOVIE_SVC_PATH
			+ "/search/findByCast";
	
	// The path to search movies by genre
	public static final String MOVIE_GENRE_SEARCH_PATH = MOVIE_SVC_PATH
			+ "/search/findByGenre";
	
	// The path to search movies by year
	public static final String MOVIE_YEAR_SEARCH_PATH = MOVIE_SVC_PATH
			+ "/search/findByYear";

	@GET(MOVIE_SVC_PATH)
	public Collection<Movie> getMovieList();

	@POST(MOVIE_SVC_PATH)
	public Void addMovie(@Body Movie m);

	@GET(MOVIE_TITLE_SEARCH_PATH)
	public Collection<Movie> findByTitle(@Query(TITLE_PARAMETER) String title);

	@GET(MOVIE_CAST_SEARCH_PATH)
	public Collection<Movie> findByActor(@Query(ACTOR_PARAMETER) String actor);
	
	@GET(MOVIE_GENRE_SEARCH_PATH)
	public Collection<Movie> findByGenre(@Query(GENRE_PARAMETER) String genre);
	
	@GET(MOVIE_YEAR_SEARCH_PATH)
	public Collection<Movie> findByYear(@Query(YEAR_PARAMETER) int year);

}
