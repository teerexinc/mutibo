package org.magnum.mobilecloud.movie;

import org.magnum.mobilecloud.movie.json.ResourcesMapper;
import org.magnum.mobilecloud.movie.repository.MovieRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

@EnableAutoConfiguration
@EnableJpaRepositories(basePackageClasses = MovieRepository.class)
@EnableWebMvc
@Configuration
@ComponentScan
public class Application extends RepositoryRestMvcConfiguration {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	@Override
	public ObjectMapper halObjectMapper(){
		return new ResourcesMapper();
	}

}
